# nvfetcher_example

This repo show how to use [nvfetcher](https://github.com/berberman/nvfetcher) to automatically update packages.  
See [Update.hs](Update.hs).  

## Usage

Edit you package's nix files, add `mySource` in input, add `inherit (mySource) pname version src;` (If use golang package, can add `vendorHash`).  
Clone this repo and change [Update.hs](Update.hs), if you use ci need change cachix. If you not use Update.hs but nvfetcher.toml, need change ci file run `nix run github:berberman/nvfetcher`.  
[how do we track upstream version updates?](https://github.com/berberman/nvfetcher#nvchecker)  
[How do we fetch the package source?](https://github.com/berberman/nvfetcher#nix-fetcher)  

## Test

run and see _sources/generated.nix  
Update.hs  
```bash
$ nix develop github:berberman/nvfetcher#ghcWithNvfetcher
$ runghc Update.hs
```

nvfetcher.toml  
```bash
$ nix run github:berberman/nvfetcher
```

## Other examples
berberman's [flakes repo](https://github.com/berberman/flakes)  
Nick Cao's [flakes repo](https://gitlab.com/NickCao/flakes/-/tree/master/pkgs)  
