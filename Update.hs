{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}

module Update (main) where

import Control.Monad (unless)
import qualified Data.Aeson as A
import Data.Default (def)
import Data.Map (Map)
import qualified Data.Map as Map
import Data.Text (Text)
import qualified Data.Text as T
import Development.Shake
import NeatInterpolation (trimming)
import NvFetcher
import NvFetcher.Config (Config (actionAfterBuild))

main :: IO ()
main = runNvFetcher' def {actionAfterBuild = generateReadme >> processAutoCommit >> processShellAutoCommit} packageSet

packageSet :: PackageSet ()
packageSet = do
  -----------------------------------------------------------------------------
  -- define $ package "apple-emoji"
  --            `sourceGitHub` ("samuelngs", "apple-emoji-linux")
  --            `fetchGitHubRelease` ("samuelngs", "apple-emoji-linux", "AppleColorEmoji.ttf")
  -----------------------------------------------------------------------------
  -- manual update and fetch in url example
  -- define $
  --   package "apple-emoji"
  --     `sourceManual` "0.0.0.20200413"
  --     `fetchUrl` const
  --       "https://github.com/samuelngs/apple-emoji-linux/releases/download/alpha-release-v1.0.0/AppleColorEmoji.ttf"
  -----------------------------------------------------------------------------
  -- pypi example
  define $ package "fastocr" `fromPypi` "fastocr"
  -----------------------------------------------------------------------------
  -- github example
  -- broken packages example
  -- define $ package "qliveplayer" `fromGitHub'` ("THMonster", "QLivePlayer", fetchSubmodules .~ True) `hasCargoLock` "src/QLivePlayer-Lib/Cargo.lock"
  -----------------------------------------------------------------------------
  -- fetch special file in github release example
  -- define $
  --   package "fcitx5-pinyin-moegirl"
  --     `sourceGitHub` ("outloudvi", "mw2fcitx")
  --     `fetchGitHubRelease` ("outloudvi", "mw2fcitx", "moegirl.dict")
  -----------------------------------------------------------------------------
  -- archlinux pkgs checker example
  -- define $
  --   package "fcitx5-pinyin-zhwiki"
  --     `sourceArchLinux` "fcitx5-pinyin-zhwiki"
  --     -- drop "0.2.4."
  --     `fetchUrl` \(T.drop 6 . coerce -> v) ->
  --       [trimming|https://github.com/felixonmars/fcitx5-pinyin-zhwiki/releases/download/0.2.4/zhwiki-$v.dict|]

-- generated commit message in GITHUB_ENV
processAutoCommit :: Action ()
processAutoCommit =
  getEnv "GITHUB_ENV" >>= \case
    Just env -> do
      changes <- getVersionChanges
      liftIO $
        unless (null changes) $
          appendFile env $
            "COMMIT_MSG<<EOF\n"
              <> case show <$> changes of
                [x] -> x <> "\n"
                xs -> "Auto update:\n" <> unlines xs
              <> "EOF\n"
    _ -> pure ()

-- generated commit message in SHELL_ENV
processShellAutoCommit :: Action ()
processShellAutoCommit =
  getEnv "SHELL_ENV" >>= \case
    Just env -> do
      changes <- getVersionChanges
      liftIO $
        unless (null changes) $
          appendFile env $
            "export " <> "COMMIT_MSG=\""
              <> case show <$> changes of
                [x] -> x <> "\n"
                xs -> "Auto update:\n" <> unlines xs
              <> "\"\n"
    _ -> pure ()

-- generated README.md
generateReadme :: Action ()
generateReadme = do
  -- we need use generated files in flakes
  command [] "git" ["add", "."] :: Action ()
  (A.decode @(Map Text Text) -> Just (Map.elems -> out)) <-
    fromStdout
      <$> command
        []
        "nix"
        [ "eval",
          "./#packages.x86_64-linux",
          "--apply",
          T.unpack [trimming|with builtins; mapAttrs (key: value: "[$${key}](${value.meta.homepage or ""}) - ${value.version}")|],
          "--json"
        ]
  template <- T.pack <$> readFile' "README_template.md"
  writeFileChanged "README.md" $ T.unpack $ template <> "\n" <> (T.unlines $ map ("* " <>) out) 
  putInfo "Generate README.md"
